import React from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import DrawerMenu from './DrawerMenu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { connect } from "react-redux";
import {toggleMenuOpen} from "./actions";

const mapStateToProps = state => {
  return { menu: state.menu };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleMenuOpen: menuOpen => dispatch(toggleMenuOpen(menuOpen))
  };
};

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    //height: 440,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
  }

  handleDrawerToggle = () => {
    this.props.toggleMenuOpen( !this.props.menu.menuOpen );
  };

  render() {
    const { classes, theme, menu } = this.props;
    const Page = menu.page;
    return (
      <div className={classes.root}>
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, this.props.menu.menuOpen && classes.appBarShift)}
        >
          <Toolbar disableGutters={!this.props.menu.menuOpen}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classNames(classes.menuButton, this.props.menu.menuOpen && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" noWrap>
              Brandon R Moore
            </Typography>
          </Toolbar>
        </AppBar>
        <DrawerMenu
          handleDrawerToggleCallback={this.handleDrawerToggle}
          menuOpen={this.props.menu.menuOpen}
        />

        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Page/>
        </main>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps,mapDispatchToProps)(App));
