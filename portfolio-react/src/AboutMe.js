import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  responsiveImage: {
    maxWidth: '100%',
    height: 'auto'
  },
  responsiveText: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '14px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '16px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '18px'
    },
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  gutterBottom: {
    marginBottom: '10px'
  }
});

class AboutMe extends React.Component {

  render() {
    const { classes, theme } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={8} direction="row">
          <Grid item xs={12}>
            <Typography variant="headline">About Me</Typography>
            <Divider className={classes.gutterBottom}/>
          </Grid>
        </Grid>
        <Grid container spacing={8} direction="row">
          <Grid item direction="column" xs={12} sm={12} md={3} className={classes.centerContainer}>
            <img className={classes.responsiveImage} src="https://a.wattpad.com/useravatar/dekuglue.256.683232.jpg"/>
          </Grid>
          <Grid item xs={12} sm={12} md={9}>
            <Typography paragraph={true} className={classes.responsiveText}>
              Chicharrones copper mug before they sold out fashion axe.
              Normcore ramps man bun small batch cardigan yr tumblr marfa forage organic copper mug.
              Brooklyn keffiyeh helvetica kickstarter, shabby chic kale chips typewriter hella direct trade.
              Af tousled intelligentsia pug vinyl.
            </Typography>
            <Typography paragraph={true} className={classes.responsiveText}>
              Health goth butcher flannel direct trade, wayfarers artisan offal tumeric chia portland.
              Ramps fixie brunch, tumblr neutra tattooed four loko.
              Semiotics cronut pinterest ramps iceland, venmo banjo sustainable drinking vinegar salvia flexitarian beard glossier seitan.
              Beard dreamcatcher banjo narwhal 3 wolf moon.
              Hot chicken iPhone dreamcatcher, man braid everyday carry quinoa taiyaki fingerstache distillery put a bird on it paleo slow-carb.
              Brooklyn marfa pitchfork, selvage lo-fi microdosing jean shorts.
              Photo booth activated charcoal shaman skateboard.
            </Typography>
            <Typography paragraph={true} className={classes.responsiveText}>
              Chicharrones copper mug before they sold out fashion axe.
              Normcore ramps man bun small batch cardigan yr tumblr marfa forage organic copper mug.
              Brooklyn keffiyeh helvetica kickstarter, shabby chic kale chips typewriter hella direct trade.
              Af tousled intelligentsia pug vinyl.
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={8} direction="row">
          <Grid item xs={12}>
            <Card elevation={3}>
              <CardHeader title="Professional Timeline">
              </CardHeader>
              <CardContent>
                <Typography>Highchart listing out various roles; Network Engineer, Software Developer, DevOps, Teacher, Manager. Similar chart to current profile</Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

AboutMe.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AboutMe);