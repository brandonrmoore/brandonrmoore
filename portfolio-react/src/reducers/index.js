import AboutMe from '../AboutMe';

const initialState = {
  menu: {
    activeView: 0,
    page: AboutMe,
    menuOpen: false
  },
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MENU_ACTIVE_VIEW':
      let {activeView,page} = action.payload;
      return {
        ...state,
        menu: {
          ...state.menu,
          activeView: activeView,
          page: page
        }
      };
    case 'TOGGLE_MENU_OPEN':
      return {
        ...state,
        menu: {
          ...state.menu,
          menuOpen: action.payload
        }
      };
    default:
      return state;
  };
};

export default rootReducer;