import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { menuItems } from "./constants/menuItems";
import {withStyles} from "@material-ui/core/styles/index";
import { setMenuActiveView } from "./actions/index";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { menu: state.menu, page: state.page };
};

const mapDispatchToProps = dispatch => {
  return {
    setMenuActiveView: menuChange => dispatch(setMenuActiveView(menuChange))
  };
};

class DrawerMenuItems extends React.Component {

  constructor(props){
    super(props);
    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  handleMenuClick(index,page){
    this.props.setMenuActiveView(index,page);
  }

  render() {
    const {menu} = this.props;

    let listItems = [];
    for(let i in menuItems){
      let thisMenuItem = menuItems[i];
      listItems.push(
        <ListItem
          key={i}
          button
          selected={menu.activeView == i}
          onClick={() => this.handleMenuClick({ activeView: i, page: thisMenuItem.target } )}
        >
          <ListItemIcon>
            <thisMenuItem.icon />
          </ListItemIcon>
          <ListItemText primary={thisMenuItem.name} />
        </ListItem>
      );
    }

    return (
      <List style={{paddingTop: '17px'}}>
        {listItems}
      </List>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(DrawerMenuItems);