import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Hidden from '@material-ui/core/Hidden';
import StarIcon from '@material-ui/icons/Star';
import ListItemText from '@material-ui/core/ListItemText';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import ReactCardFlip from 'react-card-flip';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Flippy, { FrontSide, BackSide } from 'react-flippy';


const styles = theme => ({
  root: {
    width: '100%',
    height: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  certificationCard: {
    height: '400px',
    display: 'flex'
  },
  bottomHelper: {
    borderBottom: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
  },
  topHelper: {
    borderTop: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
  },
  columnLogo: {
    flexBasis: '20%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  columnTime: {
    flexBasis: '50%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  columnTitle: {
    flexBasis: '30%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingTitle: {
    textAlign: 'center',
    [theme.breakpoints.up('xs')]: {
      fontSize: '14px',
      paddingTop: '10px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '18px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '22px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '30px'
    },
  },
  headingData: {
    textAlign: 'center',
    [theme.breakpoints.up('xs')]: {
      fontSize: '16px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '20px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '24px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '32px'
    },
  },
  responsiveText: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '14px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '16px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '18px'
    },
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  columnLarge: {
    flexBasis: '80%',
  },
  responsiveImage: {
    maxWidth: "100%",
    height: '100%'
  },
  logoContainer: {
    [theme.breakpoints.up('xs')]: {
      maxWidth: "150px",
    },
  },
  logoColumn: {
    [theme.breakpoints.up('xs')]: {
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  gutterBottom: {
    marginBottom: '10px'
  },
  certCard: {
    // maxWidth: 400,
    width: '100%',
    height: '100%'
  },
  cardMedia: {
    width: '100%',
    height: 0,
    marginTop: 0,
    paddingTop: '60%'
  },
  gridListTileBar: {
    title: {
      fontSize: 3
    },
    subtitle: {
      fontSize: 2,
    },

  },
  title: {
    fontSize: theme.typography.pxToRem(24)
  },
  modalContents: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxHeight: 1000,
    maxWidth: 800,
    width: '100%'
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});

class CertificationCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isFlipped: [
        false, false, false, false, false, false, false, false, false
      ],
      modalOpen: false
    };
    this.toggleModal = this.toggleModal.bind(this);
  }

  handleClick(index) {
    let isFlipped = this.state.isFlipped;
    isFlipped[index] = !this.state.isFlipped[index];
    this.setState({ isFlipped });
  }

  toggleModal(){
    this.setState({modalOpen: !this.state.modalOpen});
  }

  render() {
    const {classes} = this.props;

    return (
      <GridListTile>
        <img style={{height:'100%',width:'100%'}} src="https://www.guruteamirl.com/_fileupload/Image/Certification/Certification-311411343-lpic-1.jpg" alt="LPIC-1" />
        <GridListTileBar
          onClick={this.toggleModal}
          classes={{
            title: classes.title,
            subtitle: classes.subtitle
          }}
          title="LPIC-1"
          actionIcon={
            <IconButton className={classes.icon}>
              <InfoIcon />
            </IconButton>
          }
        />
        <Modal
          aria-labelledby="LPIC-1 Certification"
          aria-describedby="Larger view of certification"
          open={this.state.modalOpen}
          onClose={this.toggleModal}
        >
          <div className={`${classes.centerContainer} ${classes.modalContents}`}>
            <Card className={classes.certCard} square={false} elevation={0} raised={false}>
              <CardHeader title="LPIC-1" subheader="2015"/>

              <CardMedia image="http://brandonrmoore.com/images/lpic1.jpg" className={classes.cardMedia} />
              <CardContent>
                LPIC-1 is a Linux based certification that is the first of three certification levels. It covers basic Linux systems and administration knowledge such as network, network file systems, web services, and using the command-line interface.
                <Divider/>
                Although I have been using Linux since I first entered my professional career, I did not have any certifications around it. I was also teaching an Associate's level Linux course and wanted to incorporate much of the exam's content in order to encourage my students to take the exam once their class had been completed.
              </CardContent>

              <CardActions>
                <Button size="small" color="primary" onClick={this.toggleModal}>
                  Close
                </Button>
              </CardActions>

            </Card>
          </div>
        </Modal>
      </GridListTile>
    );
  }
}

CertificationCard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(CertificationCard);