import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import DrawerMenuItems from './DrawerMenuItems';
import withWidth from '@material-ui/core/withWidth';

const drawerWidth = 240;

const styles = theme => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
});

class DrawerMenu extends React.Component {

  render() {
    const { classes, theme, width } = this.props;
    const tempDrawer = ['xs', 'sm'];
    return (
      <Drawer
        variant={tempDrawer.indexOf(width) >= 0 ? "temporary" : "permanent"}
        classes={{
          paper: classNames(classes.drawerPaper, !this.props.menuOpen && classes.drawerPaperClose),
        }}
        open={this.props.menuOpen}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={this.props.handleDrawerToggleCallback}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <DrawerMenuItems />
        <Divider/>
      </Drawer>
    );
  }
}

DrawerMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withWidth()(withStyles(styles, { withTheme: true })(DrawerMenu));