import AboutMe from '../AboutMe';
import Employment from '../Employment';
import Education from '../Education';
import Certifications from '../Certifications';

import PersonIcon from '@material-ui/icons/Person';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import WorkIcon from '@material-ui/icons/Work';
import SchoolIcon from '@material-ui/icons/School';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import MailIcon from '@material-ui/icons/Mail';
import BuildIcon from '@material-ui/icons/Build';

export const menuItems = [
  {
    name: 'About Me',
    target: AboutMe,
    icon: PersonIcon
  },
  {
    name: 'Employment',
    target: Employment,
    icon: WorkIcon
  },
  {
    name: 'Education',
    target: Education,
    icon: SchoolIcon
  },
  {
    name: 'Certifications',
    target: Certifications,
    icon: WebAssetIcon
  },
  {
    name: 'Skills',
    target: AboutMe,
    icon: BuildIcon
  },
  {
    name: 'Resume',
    target: AboutMe,
    icon: InsertDriveFileIcon
  },
  {
    name: 'Contact',
    target: AboutMe,
    icon: MailIcon
  },
];