import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Hidden from '@material-ui/core/Hidden';
import StarIcon from '@material-ui/icons/Star';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  bottomHelper: {
    borderBottom: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
  },
  topHelper: {
    borderTop: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
  },
  columnLogo: {
    flexBasis: '20%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  columnTime: {
    flexBasis: '50%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  columnTitle: {
    flexBasis: '30%',
    display: 'flex',
    maxHeight: '50px',
    padding: `${theme.spacing.unit * 4}px ${theme.spacing.unit}px ${theme.spacing.unit * 4}px ${theme.spacing.unit}px`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingTitle: {
    textAlign: 'center',
    [theme.breakpoints.up('xs')]: {
      fontSize: '14px',
      paddingTop: '10px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '18px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '22px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '30px'
    },
  },
  headingData: {
    textAlign: 'center',
    [theme.breakpoints.up('xs')]: {
      fontSize: '16px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '20px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '24px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '32px'
    },
  },
  responsiveText: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '14px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '16px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '18px'
    },
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  columnLarge: {
    flexBasis: '80%',
  },
  responsiveImage: {
    maxWidth: "100%",
    height: 'auto'
  },
  logoContainer: {
    [theme.breakpoints.up('xs')]: {
      maxWidth: "150px",
    },
  },
  logoColumn: {
    [theme.breakpoints.up('xs')]: {
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  gutterBottom: {
    marginBottom: '10px'
  }
});

class Education extends React.Component {

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={8} direction="row">
          <Grid item xs={12}>
            <Typography variant="headline">Education</Typography>
            <Divider className={classes.gutterBottom}/>
          </Grid>
        </Grid>
        <ExpansionPanel defaultExpanded>
          <ExpansionPanelSummary expandIcon={false}>
            <Grid item container direction="row">
              <Grid item xs={12} sm={4} md={4} container direction="column" className={classes.logoColumn}>
                <div className={classes.logoContainer}>
                  <img className={classes.responsiveImage}
                       src="https://www.wgu.edu/sites/wgu.edu/files/custom_uploads/WGU-AcademicLogo_Natl_RGB_Seal_8-15.jpg"/>
                </div>
              </Grid>
              <Grid item xs={12} sm={4} md={4} container direction="column">
                <Grid container direction="row" justify="center" alignment="center">
                  <Grid item xs container direction="column" justify="center" alignment="center"
                        className={classes.centered}>
                    <Typography color="textSecondary" gutterBottom
                                className={classes.headingTitle}>Attendance</Typography>
                    <Typography color="textPrimary" gutterBottom
                                className={classNames(classes.headingData, classes.topHelper)}>
                      Aug, 2010 to Aug, 2012
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={4} md={4} container direction="column">
                <Grid container direction="row" justify="center" alignment="center">
                  <Grid item xs container direction="column" justify="center" alignment="center"
                        className={classes.centered}>
                    <Typography color="textSecondary" gutterBottom className={classes.headingTitle}>Degree</Typography>
                    <Typography color="textPrimary"
                                gutterBottom
                                className={classNames(classes.headingData, classes.topHelper)}>
                      Master of Science of Information Security and Assurance
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ExpansionPanelSummary>
          <Divider light className={classes.gutterBottom}/>
          <ExpansionPanelDetails>
            <Grid container spacing={8} direction="row">
              <Grid item direction="column" xs={12} sm={12} md={3} className={classes.centerContainer}>
                <img className={classes.responsiveImage} src="http://brandonrmoore.com/images/masters.jpg"/>
              </Grid>
              <Grid item xs={12} sm={12} md={9}>
                <Typography variant="heading">Core Classes</Typography>
                <Grid container spacing={8} direction="row">
                  <Grid item xs={12} sm={12} md={6}>
                    <List >
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                    </List>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <List >
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                      <ListItem divider >
                        <Grid item direction="column" xs={4}>
                          <Chip label="IS1446"></Chip>
                        </Grid>
                        <Grid item direction="column" xs={8}>
                          <Typography>Some IS Course</Typography>
                        </Grid>
                      </ListItem>
                    </List>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

Education.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(Education);