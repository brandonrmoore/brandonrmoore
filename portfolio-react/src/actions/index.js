export const setMenuActiveView = menuChange => ({ type: "SET_MENU_ACTIVE_VIEW", payload: menuChange });
export const toggleMenuOpen = menuOpen => ({ type: "TOGGLE_MENU_OPEN", payload: menuOpen });