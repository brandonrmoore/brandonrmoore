<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class SkillsController extends ApiController
{
    /**
    * @Route("/skills", methods="GET")
    */
    public function skillsAction()
    {
        return $this->respond([
            [
                'title' => 'The Princess Bride',
                'count' => 0
            ]
        ]);
    }
}
